from functools import lru_cache;from pydantic import (BaseSettings,Field,);
class Settings(BaseSettings):
    app_name: str = 'variable-keeper';database_user: str = Field(..., env='DATABASE_USER');database_password: str = Field(..., env='DATABASE_PASSWORD');database_host: str = Field(..., env='DATABASE_HOST');database_port: int = Field(..., env='DATABASE_PORT');database_name: str = Field(..., env='DATABASE_NAME');
    database_min_connections: int = 1;database_max_connections: int = 1;service_port: int = Field(..., env='SERVICE_PORT');gunicorn_workers: int = 1;gunicorn_worker_class: str = 'uvicorn.workers.UvicornWorker';
    @property
    def database_dsn(self):
        return 'postgres://@{}:{}/{}'.format(self.database_host,self.database_port,self.database_name,)
    class Config:env_file = ".env"
@lru_cache()
def get_settings():return Settings();
