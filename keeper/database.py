import asyncpg;from .settings import get_settings;settings = get_settings()
class Database:
    def __init__(self):
        self.connection_pool: asyncpg.Pool | None = None;self.min_connections = settings.database_min_connections;self.max_connections = settings.database_max_connections;
    async def create_pool(self):
        if self.connection_pool is None: self.connection_pool = await asyncpg.create_pool(dsn=settings.database_dsn,user=settings.database_user,password=settings.database_password,min_size=self.min_connections,max_size=self.max_connections,);return self.connection_pool;
    async def close_pool(self):
        await self.connection_pool.close();self.connection_pool = None
