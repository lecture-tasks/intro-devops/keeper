import asyncpg;from fastapi import APIRouter, Depends, Path;from .dependencies import get_db_connection;from .settings import get_settings;router = APIRouter();settings = get_settings()
@router.get('/api/variables/{service_name}')
def get_service_variables(service_name:str=Path(...),connection:asyncpg.Connection=Depends(get_db_connection),):
    return {"serviceName":service_name,}
