from .database import Database;database = Database();
async def get_db_connection():
    async with database.connection_pool.acquire() as connection:yield connection
