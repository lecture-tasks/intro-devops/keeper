import fastapi;from .api import router;from .dependencies import database;from .settings import get_settings;settings = get_settings()
def init_app():
    app = fastapi.FastAPI(title=settings.app_name,);
    @app.on_event('startup')
    async def startup(): await database.create_pool();

    @app.on_event('shutdown')
    async def shutdown(): await database.close_pool();
    app.include_router(router=router);
    return app;
